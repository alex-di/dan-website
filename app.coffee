http = require "http"
mongoose = require "mongoose"
#mongoose.connect "mongodb://localhost:27017/dan"
express = require "express"
moment = require "moment"
path = require "path"
#bp = require "body-parser"

app = express()
app.set 'port', process.env.PORT || 4055
app.set('trust proxy', true);
app.set('view engine', 'jade')
.use express.static path.join path.dirname(require.main.filename), './public'

#app.use(bp.urlencoded({ extended: false }))
#
#app.use(bp.json())
#
#Email = new mongoose.Schema
#  date:
#    default: Date.now
#    type: Date
#  email: String
#  type: String
#
#Email = mongoose.model "Log", Email


app.set('views', path.join(__dirname, 'views'));

app.get /\/$|\/benefits$|\/cases\/?.*/, (req, res) ->
  res.render "index"

server = http.createServer app
.listen app.get 'port'
