module.exports = (opts) ->

  self = riot.observable @

  data = opts.data
  data.links = ""
  for ref, i in data.refs
    ref.i =  i
    data.links += riot.render $("#template-case-link").html(), ref

  classes = ["case"]
  classes.push (if opts.index % 2 is 0 then "even" else "odd")
  classes.push data.id

  self.$el = $("<div></div>", {class: classes.join " "})

  self.$el.html riot.render $("#template-case").html(), data

  self.$el.on "click", ->
    return
    self.trigger "view"
    self.$el.height 0
    self.$el.find ".over, .info"
    .css
      width: 0
      opacity: 0

    riot.route data.link


  self.getId = ->
    data.id



#  self.$el.on "mouseover", "ul", ->
#    setTimeout ->
#      $(@).addClass "over"
#    , 250
#
#  self.$el.on "mouseout", "ul", ->
#    $(@).removeClass "over"

#
  setTimeout ->
    self.$el.addClass 'loaded'
#    plx = new Parallax self.$el.find("ul")[0]
  , 1

  return self