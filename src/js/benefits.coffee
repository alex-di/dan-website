module.exports = ->

  ready = true

  $(window).scrollTop(1)

  scrollBlock = ->
    if $(window).scrollTop() is 0
      if ready
        $(".overlay").addClass "active"
        setTimeout ->
          riot.route "/cases"
          $(window).off "scroll", scrollBlock
          ready = false
          $(".overlay").removeClass "active"
        , 800


      else

        setTimeout () ->
          $(window).scrollTop(1)
        , 100
        $(window).scrollTop(1)
        setTimeout () ->
          ready = true
        , 700
      return false


    else if $("#benefits").height() - $(window).height() - $(window).scrollTop() is 0
      if ready
        $(".overlay").addClass "active"
        setTimeout ->
          riot.route "/cases"
          $(window).off "scroll", scrollBlock
          ready = false
          $(".overlay").removeClass "active"
        , 800
      else

        setTimeout () ->
          $(window).scrollTop($("#benefits").height() - $(window).height() - 1 )
        , 100
        setTimeout () ->
          ready = true
        , 700
      return false

    else
      ready = false



  setTimeout ->
    $(window).on "scroll", scrollBlock
  , 1500

  $(".overlay").removeClass "active"