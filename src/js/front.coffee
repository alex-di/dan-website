module.exports = (opts) ->
  scroll = new ScrollMagic()

  t = $("#top-wrap")
  b = $("#bot-wrap")
  s = $("#scroller")

  bc = $(".contact-b")
  tc = $(".contact-t")

  bc.css "bottom",  $(window).height() / 2
  tc.css "top",  $(window).height() / 2

  $(window).on "resize", ->
    bc.css "bottom",  $(window).height() / 2
    tc.css "top",  $(window).height() / 2
    console.log "resize"
    console.log $(window).height()

  s.css
    "top": 750
    height: $(window).height() + 10

  scene = new ScrollScene
    triggerElement: "#scroller",
    duration: $(window).height() + 10

  .addTo scroll

  setTimeout ->
    scene.on "update", (e) ->
      switch e.target.parent().info("scrollDirection")

        when "REVERSE"
          b.animate
            height: "0%"
          , 1000

          t.animate
            height: "100%"
          , 1000
          , ->
            $(".overlay").addClass "active"
            setTimeout ->
              riot.route "/benefits"
              $(".overlay").removeClass "active"
            , 800


        when "FORWARD"
          b.animate
            height: "100%"
          , 1000

          t.animate
            height: "0%"
          , 1000
          , ->

            $(".overlay").addClass "active"
            setTimeout ->
              riot.route "/cases"
              $(".overlay").removeClass "active"
            , 800


        else
          return false
      scene.destroy true

  , 1000


  $(document).scrollTop($(window).height() / 2 + 5)
  $(".overlay").removeClass "active"
