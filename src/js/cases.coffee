Case = require "./case"
CPage = require "./case-page"
Notification = require "./notification"

module.exports = (opts) ->
  root = $("#cases")
  h = root.find(".header")

  h.height("100%")
  $(".footer").height("100%")

#  plx = new Parallax h.find("ul")[0]

  cRoot = root.find(".cases-list")
  $("#cases").removeClass "loaded"


  cases = [
    {
      id: "running"
      navColor: "black"
      bgColor: "#edf1f1"
      title: "portal 2.0"
      site: "adidas-running.ru"
      description: "Модный журнал о беговом сообществе"
      link: "/cases/running"
      images: {
        list: "/img/running/run-main-preview.jpg"
        cover: "/img/running/run-main_yellow.jpg"
        page: "/img/running/run-cover.jpg"
        nav: "/img/running/case-preview.jpg"
        navCover: "/img/running/run-case_yellow.jpg"
        logo: "/img/running/logo.png"

      }
      refs: [
        {code: "CO", addr: "#", name: "Content"}
        {code: "SM", addr: "#", name: "Social Media"}
        {code: "CT", addr: "#", name: "Creative tech"}
        {code: "AN", addr: "#", name: "Analytics"}
        {code: "UX", addr: "#", name: "Design"}
      ]
      case:
        head: "<img src='/img/running/head.jpg'>"
        description: "Модный журнал о беговом сообществе."
        task: "Перед нами стояла непростая задача, ведь строить с&nbsp;нуля всегда проще, чем что-то оптимизировать или улучшать. Требовалось понять, какие функции лишние и&nbsp;как спроектировать интерфейс и&nbsp;взаимодействие с&nbsp;пользователем таким образом, чтобы сайт был для него максимально удобен."
        solve: "Перед тем, как начать проектирование, мы&nbsp;запустили краудсорсинг-проект, в&nbsp;ходе которого сами бегуны демонстрировали нам свои нужды и&nbsp;потребности. Исследование было проведено по&nbsp;трём направлениям: контент, функционал и&nbsp;интерфейс.
        <hr>В&nbsp;каждом из&nbsp;направлений предложения участников проекта проходили верификацию и&nbsp;классифицировались в&nbsp;идеи.
        <hr>Нам удалось не&nbsp;просто поменять внешность бегового портала, но&nbsp;и&nbsp;сделать из&nbsp;ресурса о&nbsp;беге ресурс для бегунов"
        misc: "<img src='/img/running/misc.jpg'>"

    },  {
      id: "winterwinners"
      navColor: "white"
      bgColor: "#000067"
      title: "winter winners"
      link: "/cases/winterwinners"
      description: "Полноразмерный бобслей-тренажер"
      images:
        logo: "/img/running/logo.png"
        list: "/img/winterwinners/bobsleigh-main-preview.jpg"
        cover: "/img/winterwinners/bosleigh-main.jpg"
        page: "/img/winterwinners/bobsleigh-cover.jpg"
        nav: "/img/winterwinners/bobsleigh-case-preview.jpg"
        navCover: "/img/winterwinners/bobsleigh-case.jpg"
      refs: [
        {code: "CO", addr: "#", name: "Content"}
        {code: "SM", addr: "#", name: "Social media"}
        {code: "GM", addr: "#", name: "Design"}
        {code: "UX", addr: "#", name: "Design"}
      ]
      case:
        head: '<iframe src="http://vk.com/video_ext.php?oid=6599066&id=170114139&hash=8c6988b7b91a0c9d&hd=1" width="1055" height="590" frameborder="0"></iframe>'
        description: "Чемпионы этой зимы. Не&nbsp;имеющий аналогов в&nbsp;России полноразмерный бобслей-тренажер с&nbsp;возможностью посоревноваться на&nbsp;виртуальной 3D&nbsp;трассе.."
        task: "Установить ассоциативную связь между Олимпийскими играми в&nbsp;Сочи и&nbsp;компанией adidas, при том, что бренд не&nbsp;является официальным спонсором зимних Олимпийских игр."
        solve: "Мы&nbsp;решили создать полноразмерный бобслей-симулятор и&nbsp;установить его в&nbsp;торговом комплексе с&nbsp;наибольшей проходимостью, где есть магазин adidas."
        misc: $("#template-winterwinners-misc").html()
    },  {
      id: "identity"
      navColor: "white"
      bgColor: "#1a1a1a"
      title: "Adidas running identity>"
      link: "/cases/identity"
      description: "Уникальный идентификационный символ"
      images:
        list: "/img/identity/rid-main.jpg"
        cover: "/img/identity/ri.jpg"
        page: "/img/identity/rid-cover.jpg"
        nav: "/img/identity/rid-case-prw.jpg"
        navCover: "/img/identity/rid-case.jpg"
        logo: ""

      refs: [
        {code: "ID", addr: "#", name: "Identity"}
        {code: "BS", addr: "#", name: "Brand signature"}
      ]
      case:
        head: $("#template-identity-head").html()
        description: "Нашей целью было создать совершенный знак, который в конечном итоге станет кодом бега."
        task: "Мы&nbsp;нуждались в&nbsp;символе, который будет органично вписываться в&nbsp;визуальный беговой контекст, представлять роль бренда (раскрытие человеческого потенциала) и&nbsp;в&nbsp;то&nbsp;же время будет простым, легко воспроизводимым и&nbsp;узнаваемым"
        solve: 'Смысловой точкой соприкосновения между бегом и&nbsp;брендом стала идея двигаться вперед, ее&nbsp;было решено упростить в&nbsp;символическое выражение решимости и&nbsp;движения&nbsp;&mdash; в&nbsp;символе &laquo;&gt;&raquo;
          <hr>Символ прогресса. Символ энергии.<br>Символ достижения. Он наполнен энергией<br> и жизненной силой.
          <hr>
                  Он стремительный и целенаправленный,
          <br>но остается человечным и понятным для всех.'
        misc: $("#template-identity-misc").html()

    },
    {
      id: "printer-sprinter"
      navColor: "black"
      title: "printer-sprinter"
      description: "Хештег принтер"
      bgColor: "#fedc00"
      link: "/cases/printer-sprinter"
      images:
        list: "/img/printer-sprinter/main-prview.jpg"
        cover: "/img/printer-sprinter/main.jpg"
        page: "/img/printer-sprinter/prtrsprtr-cover.jpg"
        nav: "/img/printer-sprinter/prtrsprtr-case-prview.jpg"
        navCover: "/img/printer-sprinter/prtrsprtr-case.jpg"
        logo: "/img/running/logo.png"
      refs: [
        {code: "SM", addr: "#", name: "Social media"}
        {code: "CT", addr: "#", name: "Creative tech"}
      ]
      case:
        head: $("#template-printersprinter-head").html()
        description: "Printer-sprinter&nbsp;&mdash; хэштегпринтер, созданный специально для сопровождения спортивных мероприятий компании adidas. Функционал позволяет распечатать фото из&nbsp;своего instagram, указав необходимый #хэштег."
        task: "Увеличить количество упоминаний бренда<br>и мероприятий adidas в социальных сетях."
        solve: "Участники забегов adidas публиковали свои фото с&nbsp;мероприятия в&nbsp;Instagram с&nbsp;нужным хештегом прямо своего телефона. Пройдя минутную премодерацию, их&nbsp;фото выпадало из&nbsp;монолитного куба.
        <hr style='border-top:none;margin:5px 0'>Фотокарточка становилась приятным сувениром с&nbsp;забега, adidas получал освещение мероприятия в&nbsp;instagram, а&nbsp;пользовательский контент автоматически агрегировался на&nbsp;сайте adidas-running.ru. Таким образом посетители и&nbsp;пользователи портала могли в&nbsp;прямом эфире смотреть, что происходит на&nbsp;мероприятии."
        misc: "<div style='text-align:center;margin-bottom: 140px;'><img src='/img/printer-sprinter/misc.jpg'></div><img width='100%' src='/img/printer-sprinter/misc-gr.jpg'>"

    }

  ]


  if opts?

    cRoot.attr "class", "case-full"

    $("#cases .header-wrap").addClass "short"
    $("#cases .footer-wrap").addClass "short"
    c = null
    ind = null
    for item, i in cases
      if item.id is opts
        c = item
        ind = i

    unless c?
      new Notification "error", "Не найдено такого кейса"

    c = new CPage c

    prev = cases[ind - 1]
    prev = cases[cases.length - 1] unless prev?

    next = cases[ind + 1]
    next = cases[0] unless next?
    c.addMenu
      prev: prev
      next: next

    cRoot.append c.$el

  else

    $("#cases .header-wrap").removeClass "short"
    $("#cases .footer-wrap").removeClass "short"

    for item, i in cases
      c = new Case
        data: item
        index: i
      cRoot.append c.$el
      c.one "view", ->
        console.log this
        cRoot.find(".case:not(." + this.getId() + ")").css
          height: 0
          opacity: 0
    setTimeout ->
      $("#cases .header .pbg").animate
        "opacity": 1
      , 200
    , 300


  ready = true

  $(window).scrollTop(1)

  scrollBlock = ->
    if $(window).scrollTop() is 0
      if ready
        $(".overlay").addClass "active"
        setTimeout ->
          cRoot.attr "class", "cases-list"
          .html ""
          riot.route "/benefits"
          $(window).off "scroll", scrollBlock
          cRoot.find(".case").remove()
          ready = false
          $(".overlay").removeClass "active"
        , 800


      else

        setTimeout () ->
          $(window).scrollTop(1)
        , 100
        $(window).scrollTop(1)
        setTimeout () ->
          ready = true
        , 700
      return false


    else if $("#cases").height() - $(window).height() - $(window).scrollTop() is 0
      if ready
        $(".overlay").addClass "active"
        setTimeout ->
          cRoot.attr "class", "cases-list"
          .html ""
          riot.route "/benefits"
          $(window).off "scroll", scrollBlock
          cRoot.find(".case").remove()

          ready = false
          $(".overlay").removeClass "active"
        , 800



      else

        setTimeout () ->
          $(window).scrollTop($("#cases").height() - $(window).height() - 1 )
        , 100
        setTimeout () ->
          ready = true
        , 700
      return false

    else
      ready = false



  setTimeout ->
    $(window).on "scroll", scrollBlock
  , 1500
  $(".overlay").removeClass "active"