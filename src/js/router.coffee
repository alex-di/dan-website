front = require "./front"
cases = require "./cases"
benefits = require "./benefits"

module.exports = (path) ->

  console.log "Path:", path
  path.replace "http://", ""

  slice = path.slice path.indexOf("/") + 1
  id = null
  if slice.indexOf("/") > -1
    id = slice.slice slice.indexOf("/") + 1
    slice = slice.slice 0, slice.indexOf("/")


  $("#wrap").attr "class", slice

  console.log "Slice:", slice

  $(document).scrollTop(0)

  switch slice
    when "cases" then cases(id)

    when "benefits" then benefits()

    else
      $("#wrap").attr "class", "front"
      front()

