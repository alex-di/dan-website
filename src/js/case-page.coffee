module.exports = (opts) ->
  self = riot.observable @

  data = opts
  self.$el = $("<div></div>", {class: "case-page"})

  self.$el.html riot.render $("#template-case-page").html(), data

  self.addMenu = (cases) ->
    self.$el.find(".menu").html riot.render $("#template-case-nav").html(), cases

  for ref in data.refs
    self.$el.find(".case-features-inner").append riot.render $("#template-case-link-big").html(), ref
  self.$el.find(".case-features-inner").addClass "features-" + data.refs.length

  self.on "loaded", ->

    setTimeout ->
      self.$el.addClass "loaded"
      fn = window[data.id]
      fn() if fn?
    , 100

  self.trigger "loaded"