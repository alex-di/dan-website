gulp = require "gulp"
browserify = require "gulp-browserify"
watch = require "gulp-watch"
plumber = require "gulp-plumber"
coffee = require 'gulp-coffee'
rename = require 'gulp-rename'
ugly = require "gulp-uglify"

gulp.task "default", ->
  watch "src/js/*.coffee", ->
    gulp.src "src/js/main.coffee", { read: false }
    .pipe plumber()
        .pipe browserify
            insertGoals: true
            debug: true
            transform: ["coffeeify"]
            extensions: ['.coffee']

          .pipe ugly()
          .pipe rename "main.js"
          .pipe gulp.dest "./public/js"


#  watch "src/css/*.styl", ->
#    gulp.src "src/css/*.styl"
